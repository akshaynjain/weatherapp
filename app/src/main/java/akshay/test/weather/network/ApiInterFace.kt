package akshay.test.weather.network

import akshay.test.weather.model.CurrentWeatherResponse
import akshay.test.weather.model.ForecastResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterFace {

    @GET("weather")
    suspend fun getCurrentByGeoCords(
        @Query("lat")
        lat: Double,
        @Query("lon")
        lon: Double,
        @Query("units")
        units:String
    ): Response<CurrentWeatherResponse>

    @GET("geo/1.0/direct")
    suspend fun getCityNames(
        @Query("q")
        q: String,
        @Query("limit")
        limit: Int
    ): Response<CurrentWeatherResponse>

    @GET("forecast")
    suspend fun getForecastByGeoCords(
        @Query("lat")
        lat: Double,
        @Query("lon")
        lon: Double,
        @Query("units")
        units: String
    ): Response<ForecastResponse>


}