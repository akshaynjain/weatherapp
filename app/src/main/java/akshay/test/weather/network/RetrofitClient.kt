package com.akshay.wibmo.network

import akshay.test.weather.network.DefaultRequestInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory


private const val API_KEY_VALUE = "appid=7458ac71f18910f137a5ebae1bff4dd6"

class RetrofitClient private constructor(){
    private val API_BASE_URL = "https://api.openweathermap.org/data/2.5/"

    val retrofit: Retrofit

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY


        val httpClient = OkHttpClient.Builder()
            .addInterceptor(DefaultRequestInterceptor())
            .addInterceptor(interceptor)
            .build()

        val builder = Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())

        retrofit = builder.client(httpClient).build()
    }

    companion object {
        private var self: RetrofitClient? = null
        val instance: RetrofitClient
            get() {
                if (self == null) {
                    synchronized(RetrofitClient::class.java) {
                        if (self == null) {
                            self = RetrofitClient()
                        }
                    }
                }
                return self!!
            }
    }

}