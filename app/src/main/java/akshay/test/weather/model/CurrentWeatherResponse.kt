package akshay.test.weather.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class CurrentWeatherResponse(

    @Json(name = "visibility")
    val visibility: Int,

    @Json(name = "timezone")
    val timezone: Int,

    @Json(name = "main")
    val main: Main,

    @Json(name = "clouds")
    val clouds: Clouds,

    @Json(name = "sys")
    val sys: Sys,

    @Json(name = "dt")
    val dt: Int,

    @Json(name = "coord")
    val coord: Coord,

    @Json(name = "weather")
    val weather: List<WeatherItem>,

    @Json(name = "name")
    val name: String,

    @Json(name = "cod")
    val cod: Int,

    @Json(name = "id")
    val id: Int,

    @Json(name = "base")
    val base: String,

    @Json(name = "wind")
    val wind: Wind
) : Parcelable
