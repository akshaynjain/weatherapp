package akshay.test.weather.ui.home

import akshay.test.weather.databinding.FragmentHomeBinding
import akshay.test.weather.di.ViewModelFactory
import akshay.test.weather.utils.Constants
import akshay.test.weather.utils.getDateString
import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private lateinit var homeViewModel:HomeViewModel
    private lateinit var mContext: Context
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestLocationPermission()
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        observeResults()
        return binding.root
    }

    private fun requestLocationPermission() {
        val locationPermissionRequest = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { permissions ->
            when {
                permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                    // Precise location access granted.
                    requestLocationUpdate()
                }
                else -> {
                // No location access granted.
                Snackbar.make(binding.root,
                    "Denied",
                    Snackbar.LENGTH_LONG
                ).show()
            }
            }
        }
        locationPermissionRequest.launch(arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION))
    }

    private fun requestLocationUpdate(){
        val fusedLocationClient = LocationServices
            .getFusedLocationProviderClient(mContext)
        if (ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestLocationPermission()
            return
        }
        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                Toast.makeText(mContext,location.toString(),Toast.LENGTH_LONG).show()
                onLocationUpdated(location)
            }else
            //Subscribe to location changes.
                fusedLocationClient.requestLocationUpdates(
                    getLocationRequest(),
                    getLocationCallback(),
                    Looper.getMainLooper()
                )
        }
    }

    private fun onLocationUpdated(location: Location){
        homeViewModel.getWeatherAndForeCasts(location.latitude,location.longitude,Constants.Coords.METRIC)
        homeViewModel.getForecastByGeoCords(location.latitude,location.longitude, Constants.Coords.METRIC)
    }

    private fun observeResults(){
        homeViewModel.currentWeather.observe(viewLifecycleOwner) {
            Log.i("TAG", "currentWeather: $it")
            with(binding){
                textViewCityName.text= it.name
                "${it.main.temp.toInt()}°".also { textViewTemperature.text = it }
                textViewDateTime.text= getDateString(it.dt)
                textViewDescription.text=it.weather[0].description
                "${it.main.tempMin.toInt()}°/${it.main.tempMax.toInt()}°".also { textViewMinMax.text = it }
                "Feels like ${it.main.feels_like.toInt()}°".also { textViewFeelsLike.text = it }
                Picasso.get().load("https://openweathermap.org/img/wn/${it.weather[0].icon}@2x.png").into(imageViewIcon);
            }
        }
        homeViewModel.forecastResponse.observe(viewLifecycleOwner){
            Log.i("TAG", "forecastResponse: $it")
            val layoutmanager: RecyclerView.LayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,
                false)
            binding.recyclerViewForecast.layoutManager = layoutmanager
            val foreCastAdapter=ForeCastAdapter(forecastResponse = it,mContext)
            binding.recyclerViewForecast.adapter = foreCastAdapter
        }
    }

    private fun getLocationRequest(): LocationRequest {
        return LocationRequest().apply {
            interval = TimeUnit.SECONDS.toMillis(60)
            fastestInterval = TimeUnit.SECONDS.toMillis(30)
            maxWaitTime = TimeUnit.MINUTES.toMillis(2)
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    private fun getLocationCallback(): LocationCallback {
        return object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                val location = locationResult?.lastLocation ?: return
                onLocationUpdated(location)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext=context
        homeViewModel = ViewModelProvider(this, ViewModelFactory(mContext))[HomeViewModel::class.java]
    }

}