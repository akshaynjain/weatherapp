package akshay.test.weather.ui.home

import akshay.test.weather.network.ApiInterFace
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

class HomeRepository(private val api:ApiInterFace): CoroutineScope {

    override val coroutineContext: CoroutineContext get() = Dispatchers.IO

    suspend fun getWeatherAndForecasts(lat: Double, lon: Double,units: String) = api.getCurrentByGeoCords(lat,lon,units)

    suspend fun getForecastByGeoCords(lat: Double, lon: Double, units: String)= api.getForecastByGeoCords(
        lat,
        lon,
        units
    )
}