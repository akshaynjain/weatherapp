package akshay.test.weather.ui.home

import akshay.test.weather.databinding.ItemForecastBinding
import akshay.test.weather.model.ForecastResponse
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso


class ForeCastAdapter(private val forecastResponse: ForecastResponse,val context:Context):RecyclerView.Adapter<ForeCastAdapter.ViewHolder>()  {

    inner class ViewHolder( val binding: ItemForecastBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding= ItemForecastBinding.inflate(LayoutInflater.from(parent.context),parent,false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){

            binding.forecastResponse=forecastResponse.list[position]

            val path = forecastResponse.list[position].weather[0].icon
            Picasso.get().load("https://openweathermap.org/img/wn/${path}@2x.png").into(binding.imageViewForecastIcon)
//            binding.textViewDayOfWeek.text = forecastResponse.list[position].getDay()
//            binding.textViewTimeOfDay.text = forecastResponse.list[position].getHourOfDay()
//            binding.textViewTemp.text = forecastResponse.list[position].main.getTempString()
//            binding.textViewMin.text = forecastResponse.list[position].main.getTempMinString()
//            binding.textViewMax.text = forecastResponse.list[position].main.getTempMaxString()
        }
    }

    override fun getItemCount(): Int {
     return forecastResponse.list.size
    }
}