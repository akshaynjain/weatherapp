package akshay.test.weather.ui.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import akshay.test.weather.model.CurrentWeatherResponse
import akshay.test.weather.model.ForecastResponse
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class HomeViewModel(private val repository: HomeRepository) : ViewModel(), CoroutineScope {

    private val _error = MutableLiveData<String>()
    val error:MutableLiveData<String> = _error

    private val _loading = MutableLiveData<Boolean>()
    val loading:MutableLiveData<Boolean> = _loading

    private val _currentWeather = MutableLiveData<CurrentWeatherResponse>()
    val currentWeather:MutableLiveData<CurrentWeatherResponse> = _currentWeather


    private val _forecastResponse = MutableLiveData<ForecastResponse>()
    val forecastResponse:MutableLiveData<ForecastResponse> = _forecastResponse

    override val coroutineContext: CoroutineContext get() = Dispatchers.IO
    var job:Job? = null

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError(throwable.localizedMessage)
    }

    private fun onError(message:String){
        _error.postValue(message)
    }

    fun getWeatherAndForeCasts(lat: Double, lon: Double,units: String){
        job = CoroutineScope(coroutineContext + exceptionHandler).launch {
            _loading.postValue(true)
            val response = repository.getWeatherAndForecasts(lat,lon,units)
            withContext(coroutineContext){
                _loading.postValue(false)
                when{
                    response.isSuccessful -> {
                        val currentWeather: CurrentWeatherResponse = response.body() as CurrentWeatherResponse
                        _currentWeather.postValue(currentWeather)
                        Log.i("OkHttpClient", "onLocationUpdated: $currentWeather")
                    }
                    response.code()>=400 -> {
                        onError(response.errorBody().toString())
                    }else -> {
                        onError(response.message())
                    }
                }
            }
        }
    }

    fun getForecastByGeoCords(lat: Double, lon: Double,units: String){
        job = CoroutineScope(coroutineContext + exceptionHandler).launch {
            _loading.postValue(true)
            val response = repository.getForecastByGeoCords(lat,lon,units)
            withContext(coroutineContext){
                _loading.postValue(false)
                when{
                    response.isSuccessful -> {
                        val forecastResponse: ForecastResponse = response.body() as ForecastResponse
                        _forecastResponse.postValue(forecastResponse)
                        Log.i("OkHttpClient", "onLocationUpdated: $forecastResponse")
                    }
                    response.code()>=400 -> {
                        onError(response.errorBody().toString())
                    }else -> {
                    onError(response.message())
                }
                }
            }
        }
    }

}