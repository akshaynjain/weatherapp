package akshay.test.weather.di

import akshay.test.weather.network.ApiInterFace
import akshay.test.weather.ui.home.HomeRepository
import android.content.Context
import com.akshay.wibmo.network.RetrofitClient


object RepositoryFactory {
    fun createHomeRepository(context: Context) : HomeRepository {
        val githubApi = RetrofitClient.instance.retrofit.create(ApiInterFace::class.java)
        return HomeRepository(githubApi)
    }
}