package akshay.test.weather.di

import akshay.test.weather.ui.home.HomeViewModel
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelFactory(private val context: Context) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T =when(modelClass) {
        HomeViewModel::class.java->HomeViewModel(RepositoryFactory.createHomeRepository(context))
        else->throw IllegalArgumentException("unknown viewmodel class")
    }as T

}