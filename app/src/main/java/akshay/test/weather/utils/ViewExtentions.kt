package akshay.test.weather.utils

import android.view.View
import java.text.SimpleDateFormat
import java.util.*

private val simpleDateFormat = SimpleDateFormat("dd MMMM yyyy, HH:mm:ss", Locale.ENGLISH)

fun getDateString(time: Long) : String = simpleDateFormat.format(time * 1000L)

fun getDateString(time: Int) : String = simpleDateFormat.format(time * 1000L)

fun View.hide() {
    visibility = View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
}